using Microsoft.AspNetCore.Mvc;

namespace wapi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    private string AssignLabelToTemperature(int temperature)
    {
        switch (temperature)
        {
            case var t when t < -10:
                return "Very Cold";
            case var t when t >= -10 && t < 0:
                return "Cold";
            case var t when t >= 0 && t < 10:
                return "Cool";
            case var t when t >= 10 && t < 20:
                return "Warm";
            case var t when t >= 20 && t < 30:
                return "Hot";
            case var t when t >= 30:
                return "Very Hot";
            default:
                return "Unknown";
        }
    }

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public IEnumerable<WeatherForecast> Get()
    {
        var temperatureC = Random.Shared.Next(-20, 55);
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            TemperatureC = temperatureC,
            Summary = AssignLabelToTemperature(temperatureC)
        })
        .ToArray();
    }
}
